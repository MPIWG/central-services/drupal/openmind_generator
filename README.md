# Description

Following drupal console command generates configs for entitytypes, fields and entity form displays.

## Mapped field types

`src/Command/GenerateCtypeCommand.php`

```php
protected function getMappedType($type) {
  $map = [
      'date' => 'datetime',
      'text' => 'string',
      'text_long' => 'text_long',
      'boolean' => 'boolean',
      'num' => 'integer',
      'relation' => 'entity_reference'
  ];

  if (!isset($map[$type])) {
    return 'text_long';
  }
  return $map[$type];
}
```

## Hooks

```php

function my_module_openmind_generator_field_config_alter(&$params) {
  // $params['config][] = 'my-new-value';
}

function my_module_openmind_generator_field_storage_alter(&$params) {
  // $params['config][] = 'my-new-value';
}

function my_module_openmind_generator_structure_alter(&$structure) {

}
```


## Example implement a global type
```php
function my_module_openmind_generator_structure_alter(&$structure) {
  foreach( $structure as $bundle => $bundleInfo) {
    $structure[$bundle]['fields']['ismi_id'] = [
      'CONTENT-TYPE' => 'text',// Should be a type for xml
    ];
  }
}
```

## Example change a field type
```php
function my_module_openmind_generator_structure_alter($params) {
  $structure['WITNESS']['fields']['xml_field_to_change']['CONTENT-TYPE'] = 'text_long';
}
```

## Drupal console commands
```sh
drupal generate:openmind:ctype
drupal generate:openmind:ctype --xml-file /var/www/docroot/modules/contrib/openmind_generator/test/openmind.xml

```
### Drupal console options

- `--xml-file` Set source xml. Should be openmind format see `test/openmind.xml`.
- `--module` Set existing module name.
- `--prefix` Set a default prefix for all bundle and field names. Default ''
- `--all` Generate all content types without confirmation
- `--ignore-attributes` Ignore all attributes from xml type ex. old