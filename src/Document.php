<?php

namespace Drupal\openmind_generator;

class Document {

    private $counter = 0;
    private $structure = [];
    private $currentClass;
    private $notify;
    private $progress;
    private $prefix;
    private $parseNewClass = false;

    function __construct(String $prefix = '', \Closure $notify = null, \Closure $progress = null) {
      $this->parser = xml_parser_create();
      $this->notify = $notify;
      $this->prefix = $prefix;
      $this->progress = $progress;
      xml_set_object($this->parser, $this);
      xml_set_element_handler($this->parser, "startElement", "endElement");
      xml_set_character_data_handler($this->parser, "data");
    }

    function reset() {
      xml_parser_free($this->parser);
      $this->counter = 0;
      $this->structure = [];
    }

    function parseString($string) {
      return $this->parse($string);
    }

    function parseFile($file) {
      return $this->parse(file_get_contents($file));
    }

    function parse($string) {
      xml_parse($this->parser, $string);
      $structure = $this->structure;
      $this->reset();
      return $structure;
    }

    function startElement($parser, $name, $attribs) {
      if ($name == 'ENTITY') {
        $this->counter++;
        if ($this->progress != null) {
          $progress = $this->progress;
          $progress($this->counter, $attribs);
        }
        $entityName = strtolower($this->prefix . $attribs['OBJECT-CLASS']);
        $this->currentClass = $entityName;
        if (!isset($this->structure[$entityName])) {
          $this->parseNewClass = true;
          $this->structure[$entityName] = [
            'attributes' => $attribs,
            'fields' => []
          ];
        }
      } elseif ($name == 'ATTRIBUTE') {
        $this->addAttribute(strtolower($attribs['NAME']), $attribs);
      } elseif ($name == 'RELATION' && $this->currentClass) {
        $matches = [];

        preg_match('/([a-zA-Z\_]+)\[([a-zA-Z\_]+)\]/', $attribs['NAME'], $matches, PREG_OFFSET_CAPTURE, 0);
        if (count($matches) < 3) {
          return;
        }
        $this->addAttribute(strtolower($matches[1][0]), [ 'CONTENT-TYPE' => 'relation', 'BUNDLE' => $matches[2][0]]);
      }

    }

    function addAttribute($name, $attribs) {
      $attribName = $name;
      if ($this->currentClass == null
          || isset($this->structure[$this->currentClass]['fields'][$attribName])
          || !isset($attribs['CONTENT-TYPE'])) {
        return;
      }
      unset($attribs['NAME']);
      unset($attribs['ID']);
      unset($attribs['ROW-ID']);
      unset($attribs['USER']);
      unset($attribs['VERSION']);
      unset($attribs['COUNT']);
      unset($attribs['MTIME']);
      $this->structure[$this->currentClass]['fields'][$attribName] = $attribs;
    }

    function data($parser, $data) {

    }

    function endElement($parser, $name) {
      if ($name == 'ENTITY') {
        if ($this->parseNewClass && $this->currentClass != null) {
          $notify = $this->notify;
          $notify($this->structure[$this->currentClass]);
        }
        $this->currentClass = null;
        $this->parseNewClass = false;
      }
    }
}