<?php

namespace Drupal\openmind_generator\Generator;

use Drupal\Console\Core\Generator\Generator;
use Drupal\Console\Core\Generator\GeneratorInterface;

/**
 * Class GenerateCtypeGenerator
 *
 * @package Drupal\Console\Generator
 */
class GenerateCtypeGenerator extends Generator implements GeneratorInterface
{

  /**
   * {@inheritdoc}
   */
  public function generate(array $parameters)
  {
    $entity_type = $parameters['entity_type'];
    $this->addSkeletonDir($parameters['template_path'] . '/templates');
    $bundle_name = $parameters['bundle_name'];

    $this->renderFile(
      'entity_type.type.bundle_name.yml.twig',
      $parameters['module_path'] . '/' . $entity_type . '.type.' . $bundle_name . '.yml',
      $parameters
    );
  }

  public function generateFieldStorage(
      $parameters
    )
  {
    $entity_type = $parameters['entity_type'];
    $this->addSkeletonDir($parameters['template_path'] . '/templates');
    $this->renderFile(
      'field.storage.entity_type.field_name.yml.twig',
      $parameters['module_path'] . '/field.storage.'. $entity_type .'.' . $parameters['field_name'] . '.yml',
      $parameters
    );
  }

  public function generateFieldConfig(
      $parameters
    )
  {
    $entity_type = $parameters['entity_type'];
    $this->addSkeletonDir($parameters['template_path'] . '/templates');
    $this->renderFile(
      'field.field.entity_type.bundle_name.field_name.yml.twig',
      $parameters['module_path'] . '/field.field.' . $entity_type . '.' . $parameters['bundle_name'] . '.' . $parameters['field_name'] . '.yml',
      $parameters
    );
  }

  public function generateFormDisplayConfig(
    $parameters
  )
  {
    $entity_type = $parameters['entity_type'];
    $this->addSkeletonDir($parameters['template_path'] . '/templates');
    $this->renderFile(
      'core.entity_form_display.entity_type.bundle_name.default.yml.twig',
      $parameters['module_path'] . '/core.entity_form_display.' . $entity_type . '.' . $parameters['bundle_name'] . '.default.yml',
      $parameters
    );
  }

  public function generateMigation(
    $parameters
  )
  {
    $entity_type = $parameters['entity_type'];
    $this->addSkeletonDir($parameters['template_path'] . '/templates');
    $this->renderFile(
      'migrate_plus.migration_group.ismi_migration.yml',
      $parameters['module_path'] . '/migrate_plus.migration_group.ismi_migration.yml',
      $parameters
    );
    $this->renderFile(
      'migrate_plus.migration_group.ismi_migration_relations.yml',
      $parameters['module_path'] . '/migrate_plus.migration_group.ismi_migration_relations.yml',
      $parameters
    );
    $this->renderFile(
      'migrate_plus.yml.twig',
      $parameters['module_path'] . '/migrate_plus.migration.' . $entity_type . '_' . $parameters['bundle_name'] . '.yml',
      $parameters
    );
    $this->renderFile(
      'migrate_plus.relations.yml.twig',
      $parameters['module_path'] . '/migrate_plus.migration.' . $entity_type . '_' . $parameters['bundle_name'] . '_relations.yml',
      $parameters
    );
  }

}
