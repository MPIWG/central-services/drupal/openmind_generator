<?php

namespace Drupal\openmind_generator\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\Command;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Generator\GeneratorInterface;
use Drupal\Console\Extension\Manager;
use Drupal\Console\Command\Shared\ModuleTrait;
use Drupal\Core\Language\LanguageDefault;
use Drupal\openmind_generator\Document;
use Symfony\Component\Console\Input\InputOption;
use Drupal\Core\Field\FieldTypePluginManager;
use Symfony\Component\Yaml\Yaml;

/**
 * Class GenerateCtypeCommand.
 *
 * @DrupalCommand (
 *     extension="openmind_generator",
 *     extensionType="module"
 * )
 */
class GenerateCtypeCommand extends Command {

  use ModuleTrait;

  /**
   * Drupal\Console\Core\Generator\GeneratorInterface definition.
   *
   * @var \Drupal\Console\Core\Generator\GeneratorInterface
   */
  protected $generator;

  protected $extensionManager;
  protected $params;
  protected $structure;
  protected $prefix;
  protected $languageDefault;
  protected $entityType = 'node';
  protected $ignores = [];

  /**
   * Constructs a new GenerateCtypeCommand object.
   */
  public function __construct(
      GeneratorInterface $openmind_generator_generate_openmind_ctype_generator,
      Manager $module_handler,
      LanguageDefault $languageDefault,
      FieldTypePluginManager $fieldManager
  ) {
    $this->generator = $openmind_generator_generate_openmind_ctype_generator;
    $this->extensionManager = $module_handler;
    $this->languageDefault = $languageDefault;
    $this->fieldManager = $fieldManager;
    parent::__construct();
    $this->params = [];
  }
  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('generate:openmind:ctype')
      ->setDescription($this->trans('commands.generate.openmind.ctype.description'))
      ->addOption(
        'xml-file',
        null,
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.openmind.ctype.source')
      )->addOption(
        'prefix',
        null,
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.openmind.ctype.prefix')
      )->addOption(
        'module',
        null,
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.openmind.ctype.module')
      )->addOption(
        'ignore-attributes',
        null,
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.openmind.ctype.ignore-attributes')
      )->addOption(
        'all',
        null,
        InputOption::VALUE_NONE,
        $this->trans('commands.generate.openmind.ctype.module')
      )->setAliases(['omg']);
  }

 /**
  * {@inheritdoc}/
  */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $ignores = $input->getOption('ignore-attributes');
    if ($ignores && is_string($ignores)) {
      $this->ignores = explode(',', $ignores);
    }
    $sourceFile = $input->getOption('xml-file');
    $this->prefix = $input->getOption('prefix');
    $module = $input->getOption('module');

    if (!$sourceFile) {
        $sourceFile = $this->extensionManager->getModule('openmind_generator')->getPath() . '/test/openmind.xml';
        if (!$sourceFile) {
            $this->getIo()->error('Please provide source XML file');
        } else {
            $this->getIo()->info('Using test XML file');
        }
      return;
    }

    $this->params['module'] = $input->getOption('module');
    if (!$this->params['module'] || $this->extensionManager->getModule($this->params['module']) === null) {
      $this->params['module'] = $this->moduleQuestion();
    }

    $this->params['module_path'] = $this->extensionManager->getModule($this->params['module'])->getPath() . '/config/install';
    $this->params['template_path'] = $this->extensionManager->getModule('openmind_generator')->getPath();

    if ($this->prefix === null) {
      $this->prefix = $this->getPrefix();
      if ($this->prefix !== '') {
        $this->prefix .= '_';
      }
    }

    $contents = file_get_contents($sourceFile);
    $info = $this->getIo();
    $doc = new Document(
      $this->prefix,
      function($entity) use ($info) {
        $info->info('Found class ' . $entity['attributes']['OBJECT-CLASS']);
      },function($count) {
      }
    );
    $this->structure = $doc->parseString($contents);
    $this->callAlter('openmind_generator_structure', $this->structure);
  }

  /**
   *
   */
  protected function getBundleName() {
    return $this->getIo()->ask(
      // $this->trans('commands.migrate.execute.questions.db-pass')
      "Bundle machine name"
    );
  }

  /**
   *
   */
  protected function getPrefix() {
    return $this->getIo()->askEmpty(
      $this->trans('commands.generate.openmind.ctype.questions.prefix'),
      ''
    );
  }

 /**
  *
  */
  protected function getBundleLabel() {
    return $this->getIo()->ask(
      // $this->trans('commands.migrate.execute.questions.db-pass'),
      "Bundle label",
      $this->prepareBundleLabel($this->params['bundle_name'])
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $all = $input->getOption('all');
    $module = $this->params['module'];
    $entity_type = $this->entityType;
    $this->params['entity_type'] = $this->entityType;
    foreach ($this->structure as $name => $data) {
      $this->params['xml_original_type'] = $data['attributes']['OBJECT-CLASS'];
      $label = $this->prepareBundleLabel(strtolower($name));
      if ($all || $this->ask($label, $name) == "Y") {
        $this->generateType($name, $this->params);
        $fields = [];
        $configs = [
          'config' => [
          ],
          'module' => [
          ]
        ];
        $this->params['field_info_xmls'] = [];
        foreach ($data['fields'] as $field_name => $field_info) {
          $orig_field_name = $field_name;
          $field_label = $field_name;
          $field_name = $name . '_' . $field_name;
          $field_name = self::sanitizeMachineName($field_name);
          $field_type = trim($field_info['CONTENT-TYPE']);
          if (in_array($field_type, $this->ignores)) {
            continue;
          }
          $this->params['field_info_xml'] = $field_info;
          $this->params['field_info_xmls'][$orig_field_name] = [
            'drupal_field_name' => $field_name,
            'field_name' => $orig_field_name,
            'bundle' => $name,
            'entity_type' => $this->entityType,
            'xml_type' => $field_type,
            'xml_attribute' => $orig_field_name,
            'type' => $this->getMappedType($field_type),
            'target' => (isset($this->params['field_info_xml']['BUNDLE'])) ? ($this->prefix . strtolower($this->params['field_info_xml']['BUNDLE'])) : FALSE,
          ];

          if ($field_info['XML_ATTRIBUTE']) {
            $this->params['field_info_xmls'][$orig_field_name]['xml_attribute'] = $field_info['XML_ATTRIBUTE'];
          }

          $this->getIo()->info("Generate field storage for ${field_type}:${field_name}");
          $this->generateFieldStorage($name, $field_name, $field_type, $this->params);
          $this->generateFieldConfig($name, $field_name, $field_type, $field_label, $this->params);

          list($drupal_field_type, $module) = $this->getFieldTypeForDrupal($field_type);

          $type = $this->fieldManager->getDefinition($drupal_field_type);

          $fields[$field_name] = [
            'type' => $type['default_widget'],
          ];

          $configs['config'][] = "field.field.".$this->entityType.".${name}.${field_name}";
          if (!in_array($type['provider'], $configs['module'])) {
            $configs['module'][] = $type['provider'];
          }
        }
        $configs['config'][] = $this->entityType . '.type.' . $name;
        $this->generateFormDisplayConfig($name, $fields, $configs, $this->params);
        $this->generateMigrations($name, $fields, $configs, $this->params);
      }
    }
  }

  protected function getDefaultWidgetForField($field_type) {

  }

  protected function getDefaultSettings($field_type, $attribs = []) {
    $settings = [];
    switch ($field_type) {
      case 'text':
      break;
      case 'datetime':
        $settings['datetime_type'] = 'datetime';
      break;
      case 'entity_reference':
        $targetBundle = $this->prefix . strtolower($attribs['field_info_xml']['BUNDLE']);
        $settings['handler'] = 'default:' . $this->entityType;
        $settings['handler_settings'] = ['target_bundles' => [$targetBundle => $targetBundle ]];
      break;
    }
    return ['settings' => $settings];
  }

  protected function getDefaultSettingsStorage($field_type, $attribs = []) {
    $settings = [];
    switch ($field_type) {
      case 'text':
      break;
      case 'datetime':
        $settings['datetime_type'] = 'datetime';
      break;
      case 'entity_reference':
        $settings['target_type'] = $this->entityType;
      break;
    }
    return ['settings' => $settings];
  }

  protected function getMappedType($type) {
    $map = [
      'date' => 'datetime',
      'text' => 'string',
      'text_long' => 'text_long',
      'boolean' => 'boolean',
      'num' => 'integer',
      'relation' => 'entity_reference',
      'bibcite_relation' => 'entity_reference',
      'formatted' => 'text_long',
      'arabic' => 'string',
      'arabic_long' => 'text_long',
      'arabic_formatted' => 'text_long'
    ];

    if (!isset($map[$type])) {
      return 'text';
    }
    return $map[$type];
  }

  protected function getFieldTypeForDrupal($type) {
    $type = $this->getMappedType($type);
    $defs = $this->fieldManager->getDefinition($type);
    $fieldDef = [
      $type,
      [$defs['provider']]
    ];
    return $fieldDef;
  }

  protected function generateType($name, $params) {
    $label = $this->prepareBundleLabel(strtolower($name));
    $this->getIo()->info("Generate ${label} (${name})");
    $this->params['bundle_label'] = $label;
    $this->params['bundle_name'] = $name;
    $this->generator->generate($this->params);
  }

  protected function defaultGeneratorParams($hook, $bundle_name, $field_type, $field_name, &$params) {
    $params['field_type_xml'] = $field_type;
    list($field_type, $module) = $this->getFieldTypeForDrupal($field_type);
    $field_module = $module[0];
    $modules = $module;
    if ($module[0] == 'core') {
      $modules = [];
    }
    $params['settings'] = $this->getDefaultSettings($field_type, $params);
    if ($hook == 'openmind_generator_field_storage') {
      $params['settings'] = $this->getDefaultSettingsStorage($field_type);
    }
    $params['bundle_name'] = $bundle_name;
    $params['field_name'] = $field_name;
    $params['field_type'] = $field_type;
    $params['field_module'] = $field_module;
    if (!isset($params['configs'])) {
      $params['configs'] = [];
    }
    $params['modules'] = $modules;

    $params['language'] = $this->languageDefault->get()->getId();
    $params = $this->callAlter($hook, $params);
    $params['settings'] = Yaml::dump($params['settings'], 1);
  }

  protected function generateFieldStorage($bundle_name, $field_name, $field_type, $params) {

    $this->defaultGeneratorParams('openmind_generator_field_storage', $bundle_name, $field_type, $field_name, $params);
    $this->generator->generateFieldStorage($params);
  }

  protected function generateFieldConfig($bundle_name, $field_name, $field_type,  $field_label,  $params) {
    $params['configs'] = [
      'field.storage.' . $this->entityType . '.' . $field_name,
      $this->entityType . '.type.' . $bundle_name
    ];
    $this->defaultGeneratorParams('openmind_generator_field_config', $bundle_name, $field_type, $field_name, $params);
    $this->generator->generateFieldConfig($params);
  }

  protected function generateFormDisplayConfig($bundle_name, $fields, $configs,  $params) {
    $params['dependencies'] = $params['fields'] = [];
    $params['dependencies'] = $configs;
    $params['fields'] = $fields;
    $params['language'] = $this->languageDefault->get()->getId();
    $params = $this->callAlter('openmind_generator_form_display', $params);
    $this->generator->generateFormDisplayConfig($params);
  }

  protected function generateMigrations($bundle_name, $fields, $configs,  $params) {
    $params['dependencies'] = $params['dependencies'] = $params['fields'] = [];
    $params['dependencies'] = $configs;
    $params['fields'] = $fields;
    $params['language'] = $this->languageDefault->get()->getId();
    $params['source_xml'] = DRUPAL_ROOT . '/xml/openmind.xml';
    $params['source_xml_base'] = DRUPAL_ROOT . '/xml/openmind';
    $params = $this->callAlter('openmind_generator_migration', $params);
    $this->generator->generateMigation($params);
  }


  static protected function prepareFieldName($name, $prefix) {
    $name = str_replace($prefix, '', $name);
    $name = str_replace('_', ' ', $name);
    $name = ucfirst($name);
    return $name;
  }

  protected function callAlter($hook, &$data) {
    \Drupal::moduleHandler()->alter($hook, $data);
    return $data;
  }



  static protected function sanitizeMachineName($value) {
    $new_value = strtolower($value);
    $new_value = preg_replace('/[^a-z0-9_]+/', '_', $new_value);
    $new_value = preg_replace('/_+/', '_', $new_value);
    if (strlen($new_value) > 32) {
      $new_value = substr($new_value, 0, 32);
    }
    return $new_value;
  }

  protected function ask($label, $machine_name) {
    return $this->getIo()->ask(
      // $this->trans('commands.migrate.execute.questions.db-pass'),
      "Generate: ${label} (${machine_name}) [Yna]",
      "Y"
    );
  }


  protected function prepareBundleLabel($name) {
    $name = ucfirst(str_replace( $this->prefix , '',  $name));
    $name = str_replace('_', ' ', $name);
    return $name;
  }

}
